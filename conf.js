exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        browserName: 'chrome',

        chromeOptions: {
            args: ["--headless", "--disable-gpu", "--window-size=800,600"]
        }
    },
    specs: ['testFile.js'],
    jasmineNodeOpts: {
        defaultTimeoutInterval: 300000,
        isVerbose: true,
        showColors: true,
        realtimeFailure: true
    },
     baseUrl: 'https://www.grofers.com'

}