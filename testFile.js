let request = require('request');

describe('verify broken links', function () {
    it("check broken links", (done) => {
        browser.ignoreSynchronization = true;
        browser.get(browser.baseUrl)
            .then(() => {
                return element.all(by.tagName('a'));
            })
            .then((links) => {
                return links.map((link) => {
                    return link.getAttribute('href').then((url) => {
                        return url;
                    });
                });
            })
            .then((urlPromises) => {
                let promises = [];
                let deferredPromises = [];
                let message = [];

                urlPromises.forEach((urlPromise, index) => {
                    let defered = protractor.promise.defer();
                    promises[index] = defered.promise;
                    deferredPromises[index] = defered;
                });

                urlPromises.forEach((urlPromise, index) => {
                    urlPromise.then((url) => {
                        if (url) {
                            request(url, function (error, response, body) {
                                if (response) {
                                     console.log(url, response.statusCode);
                                    message.push({status: response.statusCode, url: url});
                                }

                                deferredPromises[index].resolve();
                            });
                        } else {
                            deferredPromises[index].resolve();
                        }

                    });
                });

                Promise.all(promises).then(() => {
                    console.log(message)
                    done();
                });
            })
    });

});

