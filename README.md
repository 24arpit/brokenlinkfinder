# README #

### What is this repository for? ###

* Quick summary: This repository is created to find out if there are any broken links on a webpage



### How do I set up? ###

* You can clone this repository using 'git clone https://24arpit@bitbucket.org/24arpit/brokenlinkfinder.git' 
 * Things you should have:
    * Node and NPM installed
* Once Cloned, open the terminal(on mac) or cmd prompt(on windows) and navigate to respective cloned directory
* run 'npm install'

### Libraries/Frameworks Used ###

* Protractor
* Request :Request - Simplified HTTP client
* Jasmine

### How to run tests ###

* Open the terminal(or cmd prompt) and navigate to respective directory 
* Then run: 
    * ./node_modules/.bin/webdriver-manager update
    * ./node_modules/.bin/webdriver-manager start

* Open another instance of terminal(or cmd prompt) to navigate to respective directory
* Then run:
    * ./node_modules/.bin/protractor conf.js --baseUrl='https://www.grofers.com'

### Whereabouts ###
* The configuration to run the test is  conf.js
* testFile.js has the code
### Who do I talk to? ###

* Repo owner or admin
